# Astronautes

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Step 1 

Soyez sûr d'avoir Node et Mongo installé sur votre environnement

### Step 2

Installer le module concurrently en -g 
npm i concurrently -g

### Step 3

Installer les modules du dossier front et du dossier back

### Step 4

Dans le dossier front, faire "npm run dev"