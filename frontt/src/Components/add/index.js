import { useRef, useState } from "react"
import { postAPI } from "../../libs/API"

export default function Add({ astroList, setAstroList }) {
    const resetInputName = useRef()
    const resetInputYearOld = useRef()
    const inputChanged = useRef({})
    const [error, setError] = useState(null)

    function addAstroNameOrYearOld(e) {
        inputChanged.current[e.target.name] = e.target.value
    }

    async function validateAddAstro() {
        try {
            const { name, yearOld } = inputChanged.current
            if (name === undefined || yearOld === undefined) throw "Elements manquant ou mauvais format"
            const astro = { astro: { name, yearOld } }
            const res = await postAPI(astro)
            res.astro && setAstroList([...astroList, res.astro])
            res.erreur && setError(res.erreur.message)
            resetInputName.current.value = ""
            resetInputYearOld.current.value = ""
        } catch (error) {
            setError(error)
        }
    }

    return (
        <div>
            <h2>Ajout d'un astronaute</h2>
            Nom : <input type="text" name="name" onChange={e => addAstroNameOrYearOld(e)} ref={resetInputName} />
            Age : <input type="number" name="yearOld" onChange={e => addAstroNameOrYearOld(e)} ref={resetInputYearOld} />
            <button onClick={() => validateAddAstro()}>Valider</button>
            {error && <p>{error}</p>}
        </div>)
}