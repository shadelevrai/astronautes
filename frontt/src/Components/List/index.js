import { deleteAPI } from "../../libs/API"

export default function List({ astroList, setAstroList, errorGetList, setEditable,setAstroEditable }) {

    async function removeAstro(_id) {
        const res = await deleteAPI(_id)
        res.message === "delete succes" && setAstroList(astroList.filter(ast => ast._id !== _id))
    }

    async function editeAstro(astro){
        setEditable(true)
        setAstroEditable(astro)
    }

    function astroListElement() {
        return astroList.map(astro =>
            <div key={astro._id}>
                <p> Nom : {astro.name} - Age :  {astro.yearOld} </p>
                <button onClick={()=>editeAstro(astro)}>Edité</button>
                <button onClick={() => removeAstro(astro._id)}>Supprimé</button>
                <hr/>
            </div>
        )
    }

    return <div>
        <h1>Les astronautes</h1>
        {astroList &&
            <div>
                {astroList.length === 0 ? <p>Aucun Astronaute</p> : astroListElement()}
            </div>}
        {errorGetList && <p> {errorGetList} </p>}
    </div>
}