import { useRef, useState } from "react"
import { updateAPI } from "../../libs/API"

export default function Edit({ astroEditable, setEditable }) {
    const inputChanged = useRef(astroEditable)
    const [error, setError] = useState(false)

    function astroChangeNameAndYearOld(e) {
        inputChanged.current[e.target.name] = e.target.value
    }

    async function changeAstro() {
        const res = await updateAPI(inputChanged.current)
        res.astro ? setEditable(false) : setError(true)

    }
    return <div>
        <h1>Edité un astronaute</h1>
        nom : <input name="name" defaultValue={astroEditable.name} onChange={(e) => astroChangeNameAndYearOld(e)} />
        age  : <input name="yearOld" defaultValue={astroEditable.yearOld} onChange={(e) => astroChangeNameAndYearOld(e)} />
        <button onClick={() => setEditable(false)}>Annuler</button>
        <button onClick={() => changeAstro()}>Valider</button>
        {error && <p>Le changement n'as pas été effectué</p>}
    </div>
}