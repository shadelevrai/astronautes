async function postAPI(dataAstro) {
    try {
        const res = await fetch(`${process.env.REACT_APP_HOST_SERVER}/add`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(dataAstro),
        })
        const responseData = await res.json()
        return responseData;
    } catch (error) {
        const responseData = {
            erreur: {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData;
    }
}

async function getAPI(link) {
    try {
        const res = await fetch(`${process.env.REACT_APP_HOST_SERVER}/${link}`, {
            method: "GET",
            headers: { "Content-Type": "application/json" },
        })
        const responseData = await res.json()
        return responseData
    } catch (error) {
        const responseData = {
            erreur: {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData;
    }
}

async function deleteAPI(_id) {
    try {
        const res = await fetch(`${process.env.REACT_APP_HOST_SERVER}/delete-one`, {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
            body:JSON.stringify({_id})
        })
        const responseData = await res.json()
        return responseData
    } catch (error) {
        const responseData = {
            erreur: {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData;
    }
}

async function updateAPI(astro){
    try {
        const res = await fetch(`${process.env.REACT_APP_HOST_SERVER}/update-one`,{
            method:"PATCH",
            headers: { "Content-Type": "application/json" },
            body:JSON.stringify(astro)
        })
        const responseData = await res.json()
        return responseData
    } catch (error) {
        const responseData = {
            erreur: {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData;
    }

}

export { postAPI, getAPI, deleteAPI, updateAPI }