import { useEffect, useState } from 'react';
import { getAPI } from './libs/API';
import List from './Components/List';
import Add from './Components/Add';
import Edit from './Components/Edit';

function App() {
  const [astroList, setAstroList] = useState(null)
  const [errorGetList, setErrorGetList] = useState(null)
  const [editable, setEditable] = useState(false)
  const [astroEditable, setAstroEditable] = useState(null)

  useEffect(() => {
    getAstroList()
  }, [])

  async function getAstroList() {
    const res = await getAPI("read-all")
    res.erreur ? setErrorGetList(res.erreur.message) : setAstroList(res.astro)
  }

  return (
    <div>
      {!editable ? <>
        <List astroList={astroList} setAstroList={setAstroList} errorGetList={errorGetList} setEditable={setEditable} setAstroEditable={setAstroEditable} />
        <Add astroList={astroList} setAstroList={setAstroList} />
      </>
        :
        <Edit astroEditable={astroEditable} setEditable={setEditable} />
      }

    </div>
  );
}

export default App;
