
const express = require('express');
const router = express.Router();

router.use('/astro', require('./astro'));

module.exports = router;