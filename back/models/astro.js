const mongoose = require('mongoose');
const { Schema } = mongoose;

const AstroSchema = new Schema({
    name: String,
    yearOld: Number,
});

AstroSchema.methods.toAuthJSON = function(){
    return {
        _id:this._id,
        name:this.name,
        yearOld:this.yearOld
    }
}

mongoose.model('Astro', AstroSchema);